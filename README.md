# Digital methods

A repository containing sample code and readings for a variety of digital methods.
Each directory corresponds to a different field. The readme will have links
to readings and resources for learning. The scripts will be example uses of
some of the methods and should serve as a useful base from which to start.
